/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author Yasser
 */
public class Traslado {
    private int id;//4
    private String transaccion; //TRASLADO, CONSIGNACION, BONIFICACION//43
    private long fechaTransaccion; //8
    private String referencia;//203 //Descripcion del traslado
    private String almacenOrigen;//11 {CD1,CD2,T10,T20,T30,T40,T50}
    private String ubicacionOrigen;//11 {CD1,CD2,T10,T20,T30,T40,T50}
    private String almacenDestino;//11 {CST,T10,T20,T30,T40,T50}
    private String ubicacionDestino;//11 {CST,T10,T20,T30,T40,T50}

    public Traslado() {
    }

    public Traslado(int id, String transaccion, long fechaTransaccion, String referencia, String almacenOrigen, String ubicacionOrigen, String almacenDestino, String ubicacionDestino) {
        this.id = id;
        this.transaccion = transaccion;
        this.fechaTransaccion = fechaTransaccion;
        this.referencia = referencia;
        this.almacenOrigen = almacenOrigen;
        this.ubicacionOrigen = ubicacionOrigen;
        this.almacenDestino = almacenDestino;
        this.ubicacionDestino = ubicacionDestino;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(String transaccion) {
        this.transaccion = transaccion;
    }

    public long getFechaTransaccion() {
        return fechaTransaccion;
    }

    public void setFechaTransaccion(long fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getAlmacenOrigen() {
        return almacenOrigen;
    }

    public void setAlmacenOrigen(String almacenOrigen) {
        this.almacenOrigen = almacenOrigen;
    }

    public String getUbicacionOrigen() {
        return ubicacionOrigen;
    }

    public void setUbicacionOrigen(String ubicacionOrigen) {
        this.ubicacionOrigen = ubicacionOrigen;
    }

    public String getAlmacenDestino() {
        return almacenDestino;
    }

    public void setAlmacenDestino(String almacenDestino) {
        this.almacenDestino = almacenDestino;
    }

    public String getUbicacionDestino() {
        return ubicacionDestino;
    }

    public void setUbicacionDestino(String ubicacionDestino) {
        this.ubicacionDestino = ubicacionDestino;
    }

    @Override
    public String toString() {
        return "Traslado{" + "id=" + id + ", transaccion=" + transaccion + ", fechaTransaccion=" + fechaTransaccion + ", referencia=" + referencia + ", almacenOrigen=" + almacenOrigen + ", ubicacionOrigen=" + ubicacionOrigen + ", almacenDestino=" + almacenDestino + ", ubicacionDestino=" + ubicacionDestino + '}';
    }
    
    
    
    

}
